<?php

return [

    /*Options (core, illuminate) */
    'baseModel' => 'illuminate',

    /**
     * Options (mysql, sqlite)
     */

    'driver' => 'mysql',

    'sqlite'=> [
        'database' => 'database.db',
        'charset' => 'utf8',
        'collation' => 'utfo_unicode_ci'
    ],

    'mysql' => [
        'host' => 'localhost',
        'database' => 'cursophp',
        'user' => 'root',
        'pass' => '',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci'
    ]

];