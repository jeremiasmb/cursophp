<?php

namespace App\Controllers;

use Core\BaseController;

/**
*
*/
class HomeController extends BaseController
{

	public function index()
	{
		$this->setPageTitle('HOME');
		$this->view->nome = "jeremias";
		$this->renderView('home/index','layout');
	}

}
